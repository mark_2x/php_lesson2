<?php
    require_once('functions.php');

    $navigation = [
        '#main' => 'Главная',
        '#sativa' => 'Сатива',
        '#indika' => 'Индика',
        '#gash' => 'Гашиш',
        '#contacts' => 'Контакты',
        '#protein' => 'Протеин'
    ];


?>

<html>
<head>
    <title>Green Friday</title>
    <style>
        nav {display: flex;}
        nav div {padding: 10px;}
        nav div a {color: green;}
        nav div a:hover {text-decoration: none; color: #20803b;}
    </style>
</head>
<body>

    <nav>
        <?php
            showNav($navigation);
        ?>
    </nav>

</body>
</html>